---

# Constraints

Constraints describe the job's needs to the resource manager.  The most common constraints used in gizmo are:

<table align=center width=60% border=1>
<tr><th>Constraint</th><th>Slurm Argument</th><th>Sample</th></tr>
<tr><td>time</td>           <td>-t</td><td> -t 1-6:30:00</td></tr>
<tr><td>tasks</td>          <td>-n</td><td> -n 50</td></tr>
<tr><td>cores</td>          <td>-c</td><td> -c 4 </td></tr>
<tr><td>nodes</td>          <td>-N</td><td> -N 2</td></tr>
<tr><td>memory</td>         <td>--mem</td><td> --mem=2048</td></tr>
<tr><td>disk</td>           <td>--tmp</td><td> --tmp=10240</td></tr>
</table>

 - These constraints are used to find a node with the necessary resources
 - If your job uses more memory than requested it will be killed

---

# Constraints ...

Constraints can be combined:

<pre><code>
sbatch -n 1 -c 4 -t 10:00:00 myscript.sh
</code></pre>

requests one task requiring four cores that will run in 10 hours

<pre><code>
sbatch -n 2 -N 2 -t 1-0 myscript.sh
</code></pre>

requests two tasks (default is one core per task) on two nodes that will run in one day

<pre><code>
sbatch -c 6 -t 2-12 myscript.sh
</code></pre>

requests one task (the default) requiring 6 cores running two days and twelve hours

> NOTE: if you request multiple tasks, there is no guarantee that you will get
> a single node- if you need some number of cores on the same node, use `-c` to
> request that.


