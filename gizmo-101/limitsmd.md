---

# Limits and Priority

## Accounts

- Limits are based on *accounts*, not user.  Your account may be shared by others reporting to your PI or project
- Accounts are per-PI

> Note that any limits are subject to change.  We often find it necessary to change limits due to circumstances (e.g. high utilization or system problems).

---

# Limits and Priority ...

## Aggregate Limits

As indicated earlier, limits are enforced at the account level.  All running jobs are aggregated according to their credentials- account and user- and evaluated against the system limits.

* Accounts are limited to 275 cores.
* Users are limited to 300 cores.

---

# Limits and Priority ...

## Job Constraint Limits

### Wall Time

The upper limit for any job is 30 days- we do not recommend you run jobs longer than a few days, but the capability is there.

By default, your job is allocated 3 days.  If you go over your requested time there is a grace-period: your job is allowed to go 3 days over time.

Contact SciComp to have walltime added.

---

# Limits and Priority ...

## Job Constraint Limits ...

There's no per-job limit on cores, though in aggregate your job can't use more than the per-account limit.

> Note that requesting many cores per job will increase the amount of time it will take to start the job.  A job requesting 4 cores will start almost immediately- a job needing 100 will take considerably longer.

There is also no limit on the number of nodes your job requires.

> If you request 12 tasks, that job may run on up to 12 different nodes

---

# Limits and Priority ...

## Fair-share Prioritization

When there is a backlog of work, the scheduler will prioritize the idle
workload according to the historic usage of the user and associated account.
Those who have (over the past 24 hours) been running on lots of resources will
have their idle workload prioritized behind workload from users who have not.

## Backfill

The scheduler may allow lower priority jobs to skip past higher priority jobs when that lower-priority job won't impact the higher priority job


