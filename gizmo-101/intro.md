# Introduction to Gizmo

---

# Introduction

## Before we start

* Start the NX client on your workstation and use that to open a connection to manx

* Open a terminal and clone the lesson and samples:

<pre><code>
mrg@manx:~$ <b>git clone https://bitbucket.org/atombaby/scicomp-training.git</b>
Cloning into 'scicomp-training'...
remote: Counting objects: 236, done.
remote: Compressing objects: 100% (217/217), done.
remote: Total 236 (delta 116), reused 0 (delta 0)
Receiving objects: 100% (236/236), 539.84 KiB | 381 KiB/s, done.
Resolving deltas: 100% (116/116), done.

</code></pre>


---

# Introduction

## What is Gizmo?

Gizmo is a "shared nothing" or Beowulf-style compute cluster born in mid-2012 with the combination of the hyrax and mercury clusters into one large cluster.  Its resources are available to all Center researchers for the purpose of furthering their research. 

Gizmo is operated by Scientific Computing (scicomp@fhcrc.org)

---

# Introduction ...

## Available Compute Resources

### Compute

- 336 "F class" 4 core, 32GB RAM
- 20 "G class" 16 core, 256GB RAM
- 3 "H class" 28 core 768GB RAM
- 3 "rhino class" 28 core, 384GB RAM

---

## Available Compute Resources

### Storage

Each of the compute nodes has access to:

- Isilon based "fast" storage
- OpenStack based "economy" storage
- Local and networked scratch storage

---

# Introduction ...

## Access and Use

- Available to all
- Use ssh to rhino
- Use NX to lynx, manx, or sphinx

"rhino" systems are not technically part of gizmo but are an important part of the environment.  Use rhino nodes to:

- run batch jobs on gizmo
- develop scripts and software
- *run computations that require more than 20GB RAM*

