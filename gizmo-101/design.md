---

# Designing Jobs

## Failure is Inevitable

### Plan to fail:

* do output files already exist?
* are you logging?
* where's output going?

### Patterns

Use job names to track your work

    sbatch --job-name=ref_align_32 ...

Use `--output` to save stdout/err into a file with a sensible name

    sbatch --job-name=ref_align_32 --output=ref_align_32.%J.slurm.out ...

---

# Designing Jobs ...

## Failure is Inevitable ...

### Patterns ...

Consider logging program output to a different file:

    #!/bin/bash
    #SBATCH --job-name=ref_align_32
    #SBATCH --output=ref_align_32.%J.slurm.out
    echo "Starting at $(date)"
    my_program --options=foo \
       --output=${SLURM_JOB_NAME}.bam >${SLURM_JOB_NAME}.output 2>&1 

Check for preexisting output:

    #!/bin/bash
    #SBATCH --job-name=ref_align_32
    #SBATCH --output=ref_align_32.%J.slurm.out
    if [ -f "${SLURM_JOB_NAME}.bam" ] 
    then
       echo "output file ${SLURM_JOB_NAME}.bam exists" >&2
       echo "cowardly refusing to proceed" >&2
       exit 1 # non-zero exit tells the RM we failed
    else
       ... everything else ...

---

# Designing Jobs ...

## Failure is Inevitable ...

### Patterns ...

Does your program/application support checkpointing? `tophat` does:

    #!/bin/bash
    #SBATCH --job-name=tophat_1_2
    #SBATCH --output=tophat_1_2.%J.slurm.out
    if [ -f ${SLURM_JOB_NAME}/logs/run.log ] 
    then
       echo "Trying to resume" >&2
       tophat -R ${SLURM_JOB_NAME}
    else
       tophat --output-dir ${SLURM_JOB_NAME} ...

Look for more developments on checkpointing!

---

# Designing Jobs ...

## Small is Beautiful

* Short jobs are less likely to be impacted by failure
* Jobs that don't require lots of core or memory start faster

## Patterns

If two (or more) tasks don't need or depend on each other, split the tasks into jobs

    #!/bin/bash
    #SBATCH --job-name=ref_align_32
    #SBATCH --output=ref_align_32.%J.slurm.out
    echo "Starting at $(date)"
    for IDX in {1..10}
    do
      my_program --input=${SLURM_JOB_NAME}.${IDX} --options=foo \
        --output=${SLURM_JOB_NAME}.${IDX}.bam \
        >${SLURM_JOB_NAME}.${IDX}.output 2>&1 
    done

> This runs sequentially in this script- but if we refactor into 10 jobs, we can get 10 times the throughput

---

# Designing Jobs ...

## Small is Beautiful ...

### Dependencies

* if two (or more) tasks don't need each other, but depend on each other, use job dependencies


<pre><code>
mrg@rhino$ <b>sbatch --wrap "my_program --input=foo.raw --output=foo.bam"</b>
Submitted batch job 6343866
mrg@rhino$ <b>sbatch --dependency=afterok:6343866 --wrap "my_program --input=foo.bam"</b>
</code></pre>

---

# Designing Jobs ...

### Singleton Dependencies

A job with a singleton dependency can start:

> after  any  previously launched jobs sharing the same job  name  and  user  have terminated.


---

# Designing Jobs ...

### Singleton ...

* `singleton` provides a way to run jobs in the order submitted

<pre><code>
mrg@rhino04[~/tutorial]: <b>for J in {1..3} ; do \
> sbatch --job-name=one_at_a_time \
> --dependency=singleton bin/sleeper.sh ; \
> done</b>
Submitted batch job 15016313
Submitted batch job 15016314
Submitted batch job 15016315
mrg@rhino04[~/tutorial]: <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
          15016314    campus one_at_a      mrg PD       0:00      1 (Dependency)
          15016315    campus one_at_a      mrg PD       0:00      1 (Dependency)
          15016313    campus one_at_a      mrg  R       0:03      1 gizmof78
mrg@rhino04[~/tutorial]: <b>scancel 15016313</b>
mrg@rhino04[~/tutorial]: <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
          15016315    campus one_at_a      mrg PD       0:00      1 (Dependency)
          15016314    campus one_at_a      mrg  R       0:08      1 gizmof78
mrg@rhino04[~/tutorial]: <b>scancel 15016314</b>
mrg@rhino04[~/tutorial]: <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
          15016315    campus one_at_a      mrg  R       0:02      1 gizmof78

</code></pre>

---

# Designing Jobs ...

### Singleton ...

* `singleton` can also create a "wrapup" job

<pre><code>
mrg@rhino04[~/tutorial]: <b>for J in {1..3} ; do \
> sbatch --job-name=foo bin/sleeper.sh ; done</b>
Submitted batch job 15016376
Submitted batch job 15016377
Submitted batch job 15016378
mrg@rhino04[~/tutorial]: <b>sbatch --job-name=foo --dependency=singleton bin/sleeper.sh </b>
Submitted batch job 15016379
mrg@rhino04[~/tutorial]: <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
          15016379    campus      foo      mrg PD       0:00      1 (Dependency)
          15016377    campus      foo      mrg  R       0:19      1 gizmof105
          15016378    campus      foo      mrg  R       0:19      1 gizmof105
          15016376    campus      foo      mrg  R       0:20      1 gizmof104

</code></pre>



---

# Designing Jobs ...

## Write Once - Repeat Often

Parameterize *everything*.  Well maybe not, but do it a lot...

### Patterns

Use parameters to split loops:

    #!/bin/bash
    # This is the job script...
    #
    #SBATCH --job-name=ref_align_32
    #SBATCH --output=ref_align_32.%J.slurm.out
    #
    echo "Starting at $(date)"
    my_program --input=${SLURM_JOB_NAME}.${IDX} --options=foo \
        --output=${SLURM_JOB_NAME}.${IDX}.bam \
        >${SLURM_JOB_NAME}.${IDX}.output 2>&1 

Run this with a loop in a shell session:

    for I in {1..10} ; do sbatch --export=IDX=${I} job_script.sh ; done

