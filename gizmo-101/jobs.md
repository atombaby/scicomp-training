---

# Jobs

## Terms

### Job

A job is a set of tasks with constraints

### Task

A task is a process that runs on a single node- a task may use multiple cores on the same node, but may not span nodes.

---

## Terms ...

### Constraint

A constraint describe how much and what kind of resource the tasks in a job require.  Constraints include things like the number of cores required, the length of time needed, disk or memory, and other node features.

### Node

A computational unit with some number of cores and quantity of memory that are tightly coupled.

### Core

A core is an execution slot that is capable of independent execution of processes.

---

## Terms ...

### Resource Manager

Software that manages the nodes and jobs.  The resource manager (RM) performs many tasks, but acts at the direction of the scheduler.

### Scheduler

The software that executes the usage policy for the cluster.  Prioritizes jobs in the queue and enforces usage limits.

---

# Jobs ...

Jobs are managed (i.e. queued, dispatched, run or cancelled) using a software
suite called *Slurm* which manages the workload and resources on our computing
cluster.

Slurm is short for Simple Linux Utility for Resource Management.  It is an
open-source tool born at Lawrence Livermore National Labs and now maintained by
SchedMD, LLC.

---

# Jobs ...

## What Slurm does:

- Accepts your jobs
- Maintains jobs in a queue for execution when resources are ready
- Monitors resources in the environment
- Allocates resources for your job
- Creates the environment where your job will run
- Manages any output from your job
- Reports on jobs or nodes that break
- Cleans up the node afterwards

---

# Jobs ...

## What Slurm doesn't do

- Make your single process run on multiple processors

A common misconception- asking for more processors won't impact the number of processors your job will use

Look for "number of threads" options in your program

- Won't police your job limits

A job that uses more cores or memory than requested will frequently result in over-subscribed nodes that will either perform poorly or crash.  On shared nodes, this has the potential to impact others.

