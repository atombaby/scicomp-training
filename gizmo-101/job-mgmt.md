---

# Job Management

## Showing the Job Queue

<pre><code>
mrg@rhino$ squeue -u mrg
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
           6330919    campus   my_job      mrg  R       3:46      1 gizmof87
           6330920    campus   my_job      mrg  R       3:46      1 gizmof87
           6330921    campus   my_job      mrg  R       3:46      1 gizmof83
           6330922    campus   my_job      mrg  R       3:46      1 gizmof83
           6330913    campus   my_job      mrg  R       4:14      1 gizmof90
           6330914    campus   my_job      mrg  R       4:14      1 gizmof90
           6330915    campus   my_job      mrg  R       4:14      1 gizmof90
           6330916    campus   my_job      mrg  R       4:14      1 gizmof90
           6330917    campus   my_job      mrg  R       4:14      1 gizmof87
           6330918    campus   my_job      mrg  R       4:14      1 gizmof87

</code></pre>

The command `squeue` would show all jobs in the queue, both running and pending, from all users.  As this can be a considerable number of jobs, filtering is suggested.


---

# Job Management ...

## Filtering `squeue`

These arguments filter output, limiting display:

* `-A <account>`: jobs under the indicated account name
* `-u <username>`: jobs under the indicated user name
* `-t <state>`: jobs in the indicated state.

The most common states are:

* PENDING (PD)
* RUNNING (R)

---

# Job Management ...

## Cancelling Jobs

If you need to cancel a job, you can use the command `scancel` to select the jobs to cancel.  Typically:

<pre><code>
mrg@rhino$ <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
           6332723    campus   my_job      mrg PD       0:00      1 (JobHeldAdmin)
           6332725    campus   my_job      mrg PD       0:00      1 (JobHeldAdmin)
           6332726    campus   my_job      mrg PD       0:00      1 (JobHeldAdmin)
           6332727    campus   my_job      mrg PD       0:00      1 (JobHeldAdmin)
mrg@rhino$ <b>scancel 6332727</b>
</code></pre>

---

# Job Management ...

## Cancelling Jobs ...

You can use many of the same filters used in `squeue` to select jobs when necessary.

<pre><code>
mrg@rhino$ <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
           6332723    campus   my_job      mrg PD       0:00      1 (JobHeldAdmin)
           6332725    campus   my_job      mrg PD       0:00      1 (JobHeldAdmin)
           6332726    campus   my_job      mrg PD       0:00      1 (JobHeldAdmin)
mrg@rhino$ <b>scancel -u mrg</b>
mrg@rhino$ <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
mrg@rhino$ 

</code></pre>

<table align=center width=60% border=1>
<tr>
<td><code>-u <i>user</i></code></td><td>restrict operations to jobs owned by user <i>user</i></td>
</tr>
<tr>
<td><code>-A <i>account</i></code></td><td>restrict operations to jobs owned by account <i>account</i></td>
</tr>
<tr>
<td><code>-n <i>job_name</i></code></td><td>restrict operations to jobs with the name<i>job_name</i></td>
</tr>
<tr>
<td><code>-p <i>partition</i></code></td><td>restrict operations to jobs int the partition<i>partition</i></td>
</tr>
</table>

These options may be combined in any order.

---

# Job Management ...

## Diagnosing "stuck" Jobs

If your job isn't running, first check the reason:

<pre><code>
mrg@gadget[~/tutorial]: <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
          14818292    campus sleeper.      mrg PD       0:00      1 (Resources)
</code></pre>

The reason for a job being unable to run is given in the last column of the output of squeue.  There are a number of reasons a job isn't being run:

<table align=center width=60% border=1>
<tr>
<th>Reason</th><th>Cause</th>
</tr>
<tr>
<td>Resources</td><td>Job is waiting for resources to become available</td>
</tr>
<tr>
<td>Priority</td><td>Other, higher priority, jobs are ahead of this job</td>
</tr>
<tr>
<td>AssociationResourceLimit</td><td rowspan=2>Indicates that you have reached a limit on the number of cores available to this job's account or user</td>
</tr>
<tr>
<td>QOSResourceLimit</td>
</tr>
</table>

---

# Job Management ...

## Job History

Once a job is done, its purged from `squeue`.  `sacct` queries the job database and shows running and completed jobs:


<pre><code>
mrg@rhino04[~/tutorial]: <b>sacct -u mrg</b>
       JobID    JobName  Partition    Account  AllocCPUS      State ExitCode 
------------ ---------- ---------- ---------- ---------- ---------- -------- 
14902453      singleton     campus    scicomp          1    TIMEOUT      1:0 
14902453.ba+      batch               scicomp          1  CANCELLED     0:15 
14902454      singleton     campus    scicomp          1 CANCELLED+      0:0 
14902454.ba+      batch               scicomp          1  CANCELLED     0:15 
14902455      singleton     campus    scicomp          0 CANCELLED+      0:0 
15014322        wrap.sh     campus    scicomp          1  COMPLETED      0:0 
15014322.ba+      batch               scicomp          1  COMPLETED      0:0 
</code></pre>

Only shows jobs that are or were active in the current day.

---

# Job Management ...

## Job History ...

Go back in time using `-S` and `-E` to specify a time frame:

<pre><code>
mrg@rhino04[~/tutorial]: <b>sacct -u mrg -S 2014-12-14 -E 2014-12-15</b>
       JobID    JobName  Partition    Account  AllocCPUS      State ExitCode 
------------ ---------- ---------- ---------- ---------- ---------- -------- 
14902453      singleton     campus    scicomp          1    TIMEOUT      1:0 
14902453.ba+      batch               scicomp          1  CANCELLED     0:15 
14902455      singleton     campus    scicomp          0 CANCELLED+      0:0 
14902456      singleton     campus    scicomp          0 CANCELLED+      0:0 
14902457      singleton     campus    scicomp          0 CANCELLED+      0:0 
14902458      singleton     campus    scicomp          0 CANCELLED+      0:0 
14902459      singleton     campus    scicomp          0 CANCELLED+      0:0 
</pre></code>



