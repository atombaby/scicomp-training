---

# Interactive versus Batch Computing

## Interactive

Probably the more familiar method of computing- one would start a shell and enter commands to be run.  In these sorts of jobs, output is directed to a terminal and input taken from a keyboard.

Rhino nodes provide this capability- simply log in via SSH and begin your computing.  Slurm also provides interactive computing via the `srun` command which allocates and connects you to a compute node.

---

# Interactive versus Batch Computing ...

## Batch

*Batch* computing describes a method of computing where a job is designed to run without interaction with the user.  Batch jobs are submitted to the resource manager which manages execution of the job on the node and notifies the user of the job's state as well as capturing any input or output.

Batch jobs are written as shell scripts.

---

# Interactive versus Batch Computing ...

## When to use

As a rule, we recommend batch computing whenever possible.  Using shell scripts improves reproducibility and is typically more robust.

Interactive jobs depend on terminals- so loss of a terminal (say your desktop running the SSH session to a rhino node) will result in your job being cancelled.

Interactive computing- either on the rhino nodes or via `srun` on a compute node- is suitable for small scale computing.

