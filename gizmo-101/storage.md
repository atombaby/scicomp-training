---

# Common Storage Resources

All compute nodes have the same set of mounts at the same mount points 
(including rhino compute nodes)

## Common Network Data Stores

- Your home directory : The same on all nodes and the rhinos
- The `fh` root
    - `/fh/fast/last_f`
    - `/fh/secure/last_f`
    - `/fh/scratch`

---

# Common Storage Resources

## Common Temporary Storage

These locations are accessed via paths contained within
environment variables

- `$TMPDIR` : temporary space local to the node
- `$SCRATCH/$SLURM_JOB_ID` : temporary space accessible by all nodes in the job

> NOTE These locations exist only for the duration of the job
> and are removed when the job exits.

---

# Environment Variables for Storage Management

## Use

Use the environment variables in your script:

    cp somefile.txt $TMPDIR/otherfile.txt

or

    cp somefile.txt $SCRATCH/$SLURM_JOB_ID

---

# Common Storage Resources

## Application Mounts

- `/app` : common bio-apps, organized and maintained by SciComp
- `/usr/local` : miscellaneous bio-apps

Use `modules` to enable the tools in `/app` in your environment


