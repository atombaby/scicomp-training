---

# Batch Computing in Gizmo

## The Job Script

Job scripts are little more than shell scripts. You can add Slurm options controlling the job submission

    #!/bin/bash
    #
    # Request 1 day, 2 cores, 1 node
    #SBATCH --time=1-0
    #SBATCH -n 2
    #SBATCH -N 1

    echo "Hello World"
    sleep 20
    exit

Scripts must start with the *hash-bang* line indicating it is a shell script.

Lines starting with `#SBATCH` indicate an `sbatch` option and are optional.  These are overridden by command line arguments, so adding "`--time=2-0`" to the sbatch command will override the script's option and request two days run time.

---

# Batch Computing in Gizmo ...

## Input and Output

Since batch jobs have to be self contained, we need to use other techniques to ensure that the processes are getting the data they need and that output is being properly captured.


Standard output and error from your script is captured into a file.  The file is named `slurm-<jobid>.out` by default.

<code>
<pre>
mrg@gizmo1: <b>sbatch -n 1 -t 10:00 ./output.sh</b>
Submitted batch job 1463585
mrg@gizmo1: <b>ls</b>
output.sh  slurm-1463585.out
mrg@gizmo1: <b>cat slurm-1463585.out </b>
This message goes to STDOUT
This message goes to STDERR
Any questions?
mrg@gizmo1: 
</pre>
</code>

---

# Batch Computing in Gizmo ...

## Input and Output ...

You can control the file name using "`--output=<filename>`".  A "`%j`" in this file name will be expanded to the job ID:

<code><pre>
mrg@gizmo1: <b>sbatch --output=foo.%j.out -n 1 -t 10:00 ./output.sh</b>
Submitted batch job 1463611
mrg@gizmo1: <b>ls</b>
env.sh  <i>foo.1463611.out</i>  output.sh  sleep.sh
mrg@gizmo1: <b>cat foo.1463611.out</b>
This message goes to STDOUT
This message goes to STDERR
Any questions?
</pre></code>

---

# Batch Computing in Gizmo ...

## The `wrap` Argument

If you have a simple, one line command to run you can use the `--wrap` argument and send the command directly without writing a script:

<code><pre>
mrg@rhino: <b>sbatch --wrap="python --version"</b>
Submitted batch job 15018026
mrg@rhino: <b>cat slurm-15018026.out </b>
Python 2.7.6
</pre></code>

Slurm will wrap your command into a short script and execute that. Note that the command and arguments will need to be enclosed in quotes.

---

# Batch Computing in Gizmo ...

## Variables and Parameters

There are a number of environment variables available to the job script when the job is run.  You can use these to change the behavior of your script.

* `SLURM_JOB_ID`: the ID of the job allocation
* `SLURM_JOB_NAME`: the name of the job (--job-name)
* `SLURM_JOB_CPUS_PER_NODE`: the number of cores availabe to the job on the allocated node
* `TMPDIR`: the path to the temporary directory allocated to this job
* `SLURM_SUBMIT_DIR`: the directory from which sbatch was invoked

---

# Batch Computing in Gizmo ...

## Variables and Parameters ...

### Match Cores and Threads

    bowtie -p ${SLURM_JOB_CPUS_PER_NODE} ...

### Unique Output File Names

    bowtie -p ${SLURM_JOB_CPUS_PER_NODE} \
    --al ${SLURM_JOB_NAME}.${SLURM_JOB_ID}.reads \
    --un ${SLURM_JOB_NAME}.${SLURM_JOB_ID}.unaligned \
    ${SLURM_JOB_NAME}.ebwt > ${SLURM_JOB_NAME}.sam

### Using local scratch

    cp ${SLURM_SUBMIT_DIR}/input.file ${TMPDIR}
    ${HOME}/bin/my_tool.sh -o output.foo ${SLURM_JOB_NAME}/input.file
    
---

# Batch Computing in Gizmo ...

## Variables and Parameters ...

You can pass variables into scripts:

    MYVAR1=12 sbatch --export=MYVAR1,MYVAR2=1,5,6 ...

then the script could have:

    bowtie --snpfrac ${MYVAR1} --suppress ${MYVAR2} ...

Environment variables in the current environment aren't exported into the job environment (as they are with `srun`).  Use `--export` for this.

---

# Batch Computing in Gizmo ...

## Job Arrays

Job arrays are helpful when submitting hundreds to thousands of jobs.  These arrays provide for easier and more efficient managment of those jobs.

A job array is started by specifying an array range, which is a numeric range of numbers- the array doesn't have to start at one and can have steps greater than one:

<table align=center width=80% border=1>
<tr><td>15-690</td><td>15 to 690 in increments of one</td></tr>
<tr><td>1-10:2</td><td>1 to 10 by twos starting at one (1,3,5,7,9)</td></tr>
<tr><td>1,2,3,5,8-11</td><td>1,2,3,5,8,9,10,11</td></tr>
</table>

Jobs in an array can be handled individually or <i>en masse</i>

---

# Batch Computing in Gizmo ...

## Job Arrays ...

<code><pre>
mrg@rhino: <b>sbatch --array=1-5 bin/array.sh </b>
Submitted batch job 15030036
mrg@rhino: <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
        15030036_1    campus array-de      mrg  R       0:13      1 gizmof9
        15030036_2    campus array-de      mrg  R       0:13      1 gizmof24
        15030036_3    campus array-de      mrg  R       0:13      1 gizmof37
        15030036_4    campus array-de      mrg  R       0:13      1 gizmof42
        15030036_5    campus array-de      mrg  R       0:13      1 gizmof44
mrg@rhino: <b>scancel 15030036_1</b>
mrg@rhino: <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
        15030036_2    campus array-de      mrg  R       0:28      1 gizmof24
        15030036_3    campus array-de      mrg  R       0:28      1 gizmof37
        15030036_4    campus array-de      mrg  R       0:28      1 gizmof42
        15030036_5    campus array-de      mrg  R       0:28      1 gizmof44
mrg@rhino: <b>scancel 15030036</b>
mrg@rhino: <b>squeue -u mrg</b>
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
mrg@rhino: 
</pre></code>

---

# Batch Computing in Gizmo ...

## Job Arrays ...

Information about the array is kept in environment variables:

<code><pre>
mrg@rhino: cat slurm-15030036_5.out 
Array Information
 SLURM_ARRAY_JOB_ID: 15030036
SLURM_ARRAY_TASK_ID: 5
</pre></code>


