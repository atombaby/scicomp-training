---

# Interactive

Interactive jobs are tied to the terminal in which the job has been submitted.

<pre><code>
mrg@rhino$<b> srun hostname</b>
srun: Job is in held state, pending scheduler release
srun: job 6330891 queued and waiting for resources
srun: job 6330891 has been allocated resources
gizmof13
</code></pre>

The output and errors from the process will be displayed on the terminal and input taken from the terminal, just as any process launched on the command line.

---

# Interactive ...

`srun` will run the indicated command *ntasks* times, once for each allocated core on nodes allocated in the cluster:

<pre><code>
mrg@rhino$ <b>srun -n 4 hostname </b>
srun: Job is in held state, pending scheduler release
srun: job 6330893 queued and waiting for resources
srun: job 6330893 has been allocated resources
gizmof87
gizmof83
gizmof90
gizmof13
</code></pre>

---

# Interactive ...

You can also get a shell on the allocated node(s):

<pre><code>
mrg@rhino$ <b>srun -n 1 --pty /bin/bash -il</b>
srun: Job is in held state, pending scheduler release
srun: job 6330894 queued and waiting for resources
srun: job 6330894 has been allocated resources
mrg@gizmof13[~/Work/Training]: <b>echo $SLURM_JOB_ID</b>
6330894
mrg@gizmof13[~/Work/Training]: <b>logout</b>
mrg@rhino$ 
</code></pre>

To facilitate this use, we have a number of "convenience wrappers".

---

# Interactive

## `grabnode` commands

The grabnode commands will start an interactive session on a cluster node:

* `grabcpu`: allocates 1 core
* `grabnode`: will prompt you for a number of cores to use
* `grabfullnode`: allocates 4 cores on one node
 

`grab` commands are intended for ad-hoc, casual use.

- You are limited to a total of 18 total cores using grab commands
- Cores used here do count against other limits


