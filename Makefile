
SOURCE=/home/mrg/SciComp/Training
THEME=./avalanche-custom
THEME=tango
THEME=/var/www/training/tango-custom
THEME=light
landslide=landslide

g101= \
	gizmo-101/intro.md \
	gizmo-101/jobs.md \
	gizmo-101/constraints.md \
	gizmo-101/int_v_batch.md \
	gizmo-101/interactive.md \
	gizmo-101/batch.md \
	gizmo-101/job-mgmt.md \
	gizmo-101/limitsmd.md \
	gizmo-101/design.md

all: unix-101.html gizmo-101.html

unix-101.html: unix-101/unix-101.md
	$(landslide) -i -r -t $(THEME) -d unix-101.html unix-101/unix-101.md

gizmo-101.html: $(g101)
	cat $(g101) > g101-intermediate.md
	$(landslide) -i -r -t $(THEME) -d gizmo-101.html g101-intermediate.md
	rm g101-intermediate.md

install:
	install -m 0755 -d ${HOME}/SciComp/Training
	cp -r ${SOURCE}/examples ${HOME}/SciComp/Training

clean:
	-rm gizmo-101.html
	-rm unix-101.html

realclean: clean
	-rm -rf theme

.PHONY: clean realclean

