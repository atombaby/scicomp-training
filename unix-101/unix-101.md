# Introduction to Unix

---

# Overview

> The Unix shell has been around for decades.  The shell remains one of the
most useful tools for interacting with Unix systems- while the commands are cryptic, mastery of these commands will enable you to become incredibly productive using these systems.

Some examples of how the shell can help you in your research:

<ul>
  <li>Commands can be combined using pipelines.</li>
  <li>Output can be redirected using &gt; and &lt;,
      allowing you to save input and use that as an
      input to other tools</li>
  <li>The <code>history</code> command can be used to
      view and repeat previous operations and save
      re-typing.</li>
  <li>Organize your data using directories.</li>
  <li>Use <code>grep</code> to find things in files,
      and <code>find</code> to find files.</li>
  <li>Programs can be paused, run in the background,
      or run on remote machines.</li>
  <li>Use environment variables to control its behavior.</li>
  <li>Automate repeated analysis.</li>
</ul>

---

# First Steps

* Download the NoMachine client to your Windows workstation
* Install and follow the prompts- we'll be using the hosts
  manx and lynx for accessing SciComp systems
* Run the command "unix-101-setup.sh": the sample below shows the prompt, and how you would type the command (in bold)

<pre>
manx$ <b>unix-101-setup.sh</b>
</pre>

---

# Command Basics

* Commands are two parts- a command then options and/or arguments
* Options and arguments affect the function of the command
* Spaces separate the command elements

<pre>
   command -a -b foo
   command -a --bar=foo
</pre>

Single dashes are typically used with single-letter options, the
double-dash for "longopts" with longer option names.

>This is just a convention- most commands behave this way, but
>to be sure check the documentation for the command you're using

---

# Command Basics ...

When you enter a command, you are actually running instructions in a file... so how does the shell know where to find the file you wish to run?

<pre><code>
mrg@rhino: <b>which env</b>
/usr/bin/env
</code></pre>

This command shows where the shell found the program- where the shell looks is controlled by an _environment variable_:

<pre><code>
mrg@rhino: <b>echo $PATH</b>
/usr/bin:/usr/local/bin:/bin
</code></pre>

The shell looks in the directory `/usr/bin`, then in `/usr/local/bin`, and finally in `/bin`.  First match wins and is executed by the shell.

We'll discuss environment variables in greater detail later...

---

# Command Basics...

Once you start a command, you must wait for it to return:

<pre><code>
mrg@rhino$ <b>sleep 2</b> # two seconds will pass
mrg@rhino$ 
</code></pre>

Execution can be interrupted with the "control-c" combination:

<pre><code>
mrg@rhino$ <b>sleep 20</b> # twenty seconds will pass
^C
mrg@rhino$ 
</code></pre>

---

# Processes

Running a command actually starts what the Unix kernel sees as a "process":

 * a process is an executing instance of a program
 * the process is assigned a unique number: the process identifier or PID
 * each process is managed independently by the kernel

---

# Process Control

Earlier we ran the `sleep` command and used the `control-c` key combination to stop that
process.  We can also run processes in the background:

<pre><code>
mrg@rhino$ <b>sleep 20</b>
^Z
[1]+  Stopped                 sleep 20
mrg@rhino$ <b>bg</b>
[1]+ sleep 20 &
mrg@rhino$ <b>sleep 30</b>
mrg@rhino$ 
[1]+  Done                    sleep 20
mrg@rhino$ 
</code></pre>

In this case, the first `sleep` process is running in the background, returning later.

---

# Process Control...

What we have actually done in either case is send a _signal_ to the process.

 * sending the `control-c` sends a signal that interrupts the process
 * the `control-z` sends a signal that suspends the process

Many signals exist- those two are the most commonly used

---

# Process Control...

the `ps` command will show you running processes:

<pre><code>
mrg@rhino$ <b>ps</b>
  PID TTY          TIME CMD
25694 pts/2    00:00:00 bash
28386 pts/2    00:00:00 sleep
28393 pts/2    00:00:00 ps
mrg@rhino$ <b>bg</b>
[1]+ sleep 20 &
mrg@rhino$ <b>ps -f</b>
UID        PID  PPID  C STIME TTY          TIME CMD
mrg        928 25694  0 07:39 pts/2    00:00:00 sleep 20
mrg       2019 25694  0 07:39 pts/2    00:00:00 ps -f
mrg      25694 25691  0 06:54 pts/2    00:00:00 -bash
</code></pre>

The important number is the `PID`, which is the unique identifier for that particular process

---

# Process Control...

If you start a process and need to stop it, use the `kill` and `pkill` commands:

<pre><code>
mrg@rhino$ <b>sleep 200 &</b>
[1] 9843
mrg@rhino$ ps
  PID TTY          TIME CMD
 9843 pts/2    00:00:00 sleep
 9853 pts/2    00:00:00 ps
25694 pts/2    00:00:00 bash
mrg@rhino$ <b>kill 9843</b> #<- specify the PID of the sleep process
mrg@rhino$ <b>ps</b>
  PID TTY          TIME CMD
 9972 pts/2    00:00:00 ps
25694 pts/2    00:00:00 bash
[1]+  Terminated              sleep 200
mrg@rhino$ 

</code></pre>

---

# Process Control...

The command `pkill` allows you to stop processes using the name of the process, not the PID.

<pre><code>
mrg@rhino$ <b>sleep 200 &</b>
[1] 11483
mrg@rhino$ <b>pkill sleep</b>
[1]+  Terminated              sleep 200
</code></pre>

---

# Process Control...

but `pkill` is greedy- it will kill every process matching the name:

<pre><code>
mrg@rhino$ <b>sleep 200 &</b>
[1] 12993
mrg@rhino$ <b>sleep 200 &</b>
[2] 13006
mrg@rhino$ <b>sleep 200 &</b>
[3] 13012
mrg@rhino$ <b>ps</b>
  PID TTY          TIME CMD
12993 pts/2    00:00:00 sleep
13006 pts/2    00:00:00 sleep
13012 pts/2    00:00:00 sleep
13016 pts/2    00:00:00 ps
25694 pts/2    00:00:00 bash
mrg@rhino$ <b>pkill sleep</b>
[1]   Terminated              sleep 200
[2]-  Terminated              sleep 200
[3]+  Terminated              sleep 200
</code></pre>

---

# Some Shell Shortcuts 

## History

`bash` retains a record of all the commands you've entered:

<pre><code>
mrg@rhino: <b>history |tail</b>
  529  cd ..
  530  mkdir fastq
  531  cd fastq
  532  wget http://brianknaus.com/software/srtoolbox/s_4_1_sequence80.txt
  533  less s_4_1_sequence80.txt 
  534  grep TCGTA s_4_1_sequence80.txt
  535  cd ../pd
  536  cd ../pdb
  537  ls
  538  history |tail
</code></pre>

With command history enabled you can use the arrows to scroll through your history, repeat and edit previous commands.

Entering `<ctrl>-r` will start a history search- enter command snippets to show matching commands in your history

You can also access previous commands using the arrows on your keyboard.

---

# Some Shell Shortcuts ...

## Tab Completion

Use the `tab` key while typing to complete commands, arguments (sometimes), and file names.

- A single tab will match and complete a single match
- Two tabs will display all matches

---

# Files and Directories

Unix uses a heirarchical file system to store your data.  It can be visualized as a tree with directories being the branches.  Off branches there can be files (leaves) and directories (more branches).

![hfs](assets/unix-101/hfs.svg)

Files and directores are represented by the object names from the root to the node, with a slash "`/`" separating each.

Some paths from the diagram above:

- `/usr`
- `/home/davros`
- `/home/mrg`

---

# Files and Directories ...

Navigating the file system starts with finding your current directory:

<pre><code>
mrg@rhino: <b>pwd</b>
/home/mrg
mrg@rhino:
</code></pre>

`pwd` is shorthand for *print working directory*.  To view contents, use `ls`:

<pre><code>
mrg@rhino: <b>ls</b>
bin  data  README
</code></pre>

More information can be had by adding options to `ls`:

<pre><code>
mrg@rhino: <b>ls -F</b>
bin/  data/  README
mrg@rhino: <b>ls -l</b>
total 12
drwxr-xr-x 2 mrg wheel 4096 Feb 18 18:26 bin
drwxr-xr-x 2 mrg wheel 4096 Feb 18 18:26 data
-rw-r--r-- 1 mrg wheel   35 Feb 18 18:27 README
</code></pre>

The `-F` adds an indicator of the file type (a "/" for directories in this case).  The `-l` gives a full listing which includes times and permissions.

---

# Files and Directories ...

To move around in the file system use the command `cd` (for *change directory*).  `cd` takes an argument- the destination directory:

<pre><code>
mrg@rhino: <b>cd </b>
mrg@rhino: <b>cd unix-101</b>
mrg@rhino: <b>ls</b>
data  examples
mrg@rhino: <b>pwd</b>
/home/mrg/unix-101
</code></pre>

`cd` without any arguments will return you to your home directory:

<pre><code>
mrg@rhino: <b>pwd</b>
/home/mrg/unix-101
mrg@rhino: <b>cd</b>
mrg@rhino: <b>pwd</b>
/home/mrg
</code></pre>

---

# Files and Directories ...

Two special directories are "`.`" and "`..`".  The first represents the current directory, the latter the directory above the current directory:

<pre><code>
mrg@rhino: <b>pwd</b>
/home/mrg/unix-101
mrg@rhino: <b>ls .</b>
data  examples
mrg@rhino: <b>ls ..</b>
<i> home directory contents </i>
</code></pre>

These two directories are a key component of *relative* paths.  Relative paths are paths that are *relative* to the current directory:

<pre><code>
mrg@rhino: <b>cd data</b>
mrg@rhino: <b>ls ..</b>
data  examples
mrg@rhino: <b>ls ../..</b>
<i> home directory contents </i>
</code></pre>

---

# Files and Directories ...

## Relative Paths

 * makes your work portable
 * not dependent on the layout of the file system
 * not dependent on which system you are on
 * enable you to move and share data, process, and analyses

---

# Managing Files and Directories

## Creating Directories

**mkdir** creates a directory:

<pre><code>
mrg@rhino: <b>ls</b>
bin data README
mrg@rhino: <b>mkdir tmp</b>
mrg@rhino: <b>ls</b>
bin data README tmp
</code></pre>

Add "`-p`" to create all of the path elements

<pre><code>
mrg@rhino: <b>mkdir tmp/foo/bar</b>
mkdir: cannot create directory `tmp/foo/bar': No such file or directory
mrg@rhino: <b>mkdir -p tmp/foo/bar</b>
mrg@rhino: <b>touch tmp/foo/bar/file</b>
mrg@rhino: <b>ls tmp/foo/bar</b>
file
</code></pre>

---

# Managing Files and Directories...

## Move and Copy Files

**cp &lt;source> &lt;target>**

Copies files. If the target is a directory, it'll
retain the source file name.

**mv &lt;source> &lt;target>**

Moves files and directories. Also used to rename files

<pre><code>
mrg@rhino: <b>cp bin/env.sh tmp</b>
mrg@rhino: <b>ls tmp</b>
env.sh
mrg@rhino: <b>mv tmp/env.sh tmp/test.sh</b>
mrg@rhino: <b>ls tmp</b>
test.sh
</code></pre>

---

# Managing Files and Directories...

## Clean Up

**rm &lt;target>**

Removes files. 

**rmdir &lt;target>**

Removes empty directories.

<pre><code>
mrg@rhino: <b>ls tmp</b>
test.sh
mrg@rhino: <b>rmdir tmp</b>
rmdir: failed to remove `tmp': Directory not empty
mrg@rhino: <b>rm tmp/test.sh </b>
mrg@rhino: <b>rmdir tmp</b>
mrg@rhino: <b>ls</b>
bin  data  README
</code></pre>

---

# Managing Files and Directories...

## Clean Up ...

Add "`-r`" to `rm` and it will remove the target
recursively- the target and everything below. Use
carefully:

<pre><code>
mrg@rhino: <b>rmdir tmp</b>
rmdir: failed to remove `tmp': Directory not empty
mrg@rhino: <b>rm -r tmp</b>
mrg@rhino: <b>ls</b>
bin  data  README
</code></pre>

---

# Managing Files and Directories...

## Wildcards and Globbing

Wildcards are handy when there's lots of files or directories to manage.  Wildcards are special characters that are expanded by the shell before your command is executed.  Those characters are matched to files on the file system, with all of the matching file names replacing the original wild card

<pre><code>
mrg@rhino$ <b>ls</b>
decane.pdb  dna-ligase.pdb  heptane.pdb  methane.pdb
mrg@rhino$ <b>ls d*.pdb</b>
decane.pdb  dna-ligase.pdb
mrg@rhino$ <b>ls *e.pdb</b>
decane.pdb  dna-ligase.pdb  heptane.pdb  methane.pdb
mrg@rhino$ <b>ls *ne.pdb</b>
decane.pdb  heptane.pdb  methane.pdb
</code></pre>

The asterisk (or *splat*) is a special character that matches anything including nothing.  `*.pdb` would match any file that ending with ".pdb"

---

# Managing Files and Directories...

## Wildcards and Globbing ...

We can see a little better what's actually happening with `set -x` to show the command being run:

<pre><code>
mrg@rhino$ <b>set -x</b>
mrg@rhino$ <b>ls d*.pdb</b>
+ ls decane.pdb dna-ligase.pdb
decane.pdb  dna-ligase.pdb
mrg@rhino$ <b>ls *e.pdb</b>
+ ls decane.pdb dna-ligase.pdb heptane.pdb methane.pdb
decane.pdb  dna-ligase.pdb  heptane.pdb  methane.pdb
mrg@rhino$ <b>set +x</b>
+ set +x
</code></pre>

---

# Managing Files and Directories...

## Wildcards and Globbing ...

`?` is slightly less "greedy" than the splat operator. The question mark matches any single character:

<pre><code>
mrg@rhino$ <b>ls d????e.pdb</b>
decane.pdb
mrg@rhino$ <b>ls d???e.pdb</b>
ls: cannot access d???e.pdb: No such file or directory
</code></pre>

---

# Managing Files and Directories...

## Wildcards and Globbing ...

Those are useful, but sometimes more refined patterns are necessary. Ranges can be matched by enclosing them in brackets:

<pre><code>
mrg@rhino$ <b>cd data/illumina/141121_SN367_0473_VAHB3VADXX/Data/Intensities</b>
mrg@rhino$ <b>ls -d L00[1-2]/C99.1</b>
L001/C99.1  L002/C99.1
mrg@rhino$ <b>ls -ld L00[1-2]/C99.1</b>
drwxrwxr-x 2 mrg g_mrg 4096 Dec  4 10:49 L001/C99.1
drwxrwxr-x 2 mrg g_mrg 4096 Dec  4 10:50 L002/C99.1
mrg@rhino$ <b>ls -d [A-Z][0-9][0-9][1-2]/C99.1</b>
L001/C99.1  L002/C99.1
</code></pre>

---

# Managing Files and Directories...

## Special Characters

Some files may contain characters that are special to the shell:

 * meaning that that shell will interpret and act on those characters, changing the command
 * special characters are mostly the non-alphanumerics, quotes, spaces, slashes, brackets, braces, et alia.
 * dots, dashes, and underscore are all OK

Special characters must be escaped.. suppose we want a file name with spaces:

<pre><code>
mrg@rhino: touch this is a long file name
+ touch this is a long file name
mrg@rhino: ls -l
+ ls -l
total 0
-rw-rw-r-- 1 mrg g_mrg 0 Dec 17 10:28 a
-rw-rw-r-- 1 mrg g_mrg 0 Dec 17 10:28 file
-rw-rw-r-- 1 mrg g_mrg 0 Dec 17 10:28 is
-rw-rw-r-- 1 mrg g_mrg 0 Dec 17 10:28 long
-rw-rw-r-- 1 mrg g_mrg 0 Dec 17 10:28 name
-rw-rw-r-- 1 mrg g_mrg 0 Dec 17 10:28 this
</code></pre>

we have to escape or otherwise protect spaces as the shell uses the space to separate arguments

---

# Managing Files and Directories...

## Special Characters ...

Protect special characters with quotes or a backslash preceding the special character:

<pre><code>
mrg@rhino: touch "this is a long file name"
+ touch 'this is a long file name'
mrg@rhino: ls -l
+ ls -l
total 0
-rw-rw-r-- 1 mrg g_mrg 0 Dec 17 10:30 this is a long file name
mrg@rhino: touch protect\ the\ spaces\!
+ touch 'protect the spaces!'
mrg@rhino: ls -l
+ ls -l
total 0
-rw-rw-r-- 1 mrg g_mrg 0 Dec 17 10:31 protect the spaces!
-rw-rw-r-- 1 mrg g_mrg 0 Dec 17 10:30 this is a long file name
</code></pre>

---

# Managing Files and Directories...

## Brace Expansion

Brace expansion is similar, but doesn't require a matching file name to be expanded:

<pre><code>
mrg@rhino$ <b>echo {one,two,three}</b>
one two three
mrg@rhino$ <b>echo a{one,two,three}</b>
aone atwo athree
mrg@rhino$ <b>file {decane,heptane,does_not_exist}.pdb</b>
+ file decane.pdb heptane.pdb does_not_exist.pdb
decane.pdb:         ASCII text
heptane.pdb:        ASCII text
does_not_exist.pdb: ERROR: cannot open `does_not_exist.pdb' (No such file or directory)
</code></pre>

<pre><code>
mrg@rhino$ <b>echo {1..5}</b>
1 2 3 4 5
</code></pre>

<pre><code>
mrg@rhino$ <b>ls bin/env.{sh,env}</b>
bin/env.env  bin/env.sh
</code></pre>

---

# Managing Files and Directories...

## Permissions & Ownership

All files have permissions associated with them:

<pre><code>
mrg@rhino$ <b>ls -l README </b>
-rw-rw-r-- 1 mrg g_mrg 399 Apr 21 07:13 README
</code></pre>

In this are the permission bits (read, write, execute bits) and the account and group to whom those bits apply.

In this case:

* owner `mrg` has read and write permissons
* the group `g_mrg` has read and write permissions
* everybody else (`other`) has only read permissions

---

# Managing Files and Directories ...

## Permissions & Ownership ...

The execute bit enables the shell to open and execute (run) the file's contents within the shell:

<pre><code>
mrg@rhino$ <b>ls -la bin</b>
total 24
drwxrwxr-x 2 mrg g_mrg 4096 Apr 21 07:13 .
drwxrwxr-x 4 mrg g_mrg 4096 Apr 21 07:57 ..
-rwxr-xr-x 1 mrg g_mrg   60 Apr 21 07:13 env.env
-rwxr-xr-x 1 mrg g_mrg  132 Apr 21 07:13 env.sh
-rwxr-xr-x 1 mrg g_mrg  143 Apr 21 07:13 hello.sh
-rwxr-xr-x 1 mrg g_mrg  326 Apr 21 07:13 largertask.sh
-rwxr-xr-x 1 mrg g_mrg  271 Apr 21 07:13 sleeper.sh
mrg@rhino$ <b>bin/env.sh</b>
TMPDIR
SCRATCH_LOCAL /loc/scratch
SCRATCH /mnt/scratch-m2/scratch/gizmo
DELETE30 /mnt/scratch-m2/scratch/delete30
</code></pre>

---

# Managing Files and Directories ...

## Permissions & Ownership ...

Directories also have permissions:

<pre><code>
mrg@rhino$ <b>ls -l </b>
total 12
drwxrwxr-x 2 mrg g_mrg 4096 Apr 21 07:13 bin
drwxrwxr-x 4 mrg g_mrg 4096 Apr 21 07:13 data
-rw-rw-r-- 1 mrg g_mrg  399 Apr 21 07:13 README
</code></pre>

Here you see the `execute` bit set.  For directories:

* `read` indicates that the possessor has permissions to view directory contents
* `write` indicate that the possessor has permissions to create, edit, or delete directory contents
* `execute` indicates that the posessor can 
    * enter the directory (i.e. `cd` into the directory)
    * view attributes of objects in thd directory

> This can get strange- unless you have special needs, simply
> add read and execute permissions together.

---

# Managing Files and Directories ...

## Setting Permissions 

### <code>chmod</code>

This command will change mode for a file:

<pre><code>
mrg@rhino: <b>ls -l README </b>
-rw-rw-r-- 1 mrg g_mrg 399 Dec 15 06:53 README
mrg@rhino: <b>chmod g-w README </b>
mrg@rhino: <b>ls -l README </b>
-rw-r--r-- 1 mrg g_mrg 399 Dec 15 06:53 README
</code></pre>

Structure is "chmod" plus:

 - the entities you wish to modify (user, group, other)
 - how you wish to set the mode (add, remove, set)
 - what you wish to set (read, write, execute)

---

# Managing Files and Directories ...

## Setting Group Ownership

If you are collaborating with others, you may need to set the group for the file:

<pre><code>
mrg@rhino: <b>ls -l README </b>
-rw-r--r-- 1 mrg g_mrg 399 Dec 15 06:53 README
mrg@rhino: <b>chgrp hyrax_admin README</b>
mrg@rhino: <b>ls -l README </b>
-rw-r--r-- 1 mrg hyrax_admin 399 Dec 15 06:53 README
</code></pre>

You must be a member of the group to change group ownership.

<pre><code>
mrg@rhino: <b>groups</b>
g_mrg solexa g_rcs hyrax_admin SciComp 
</code></pre>

---

# Managing Files and Directories ...

## Default Permissions

The default permissions for newly created files and directories is controlled by the umask.

<pre><code>
mrg@rhino: <b>umask</b>
0002
mrg@rhino: <b>umask -S</b>
u=rwx,g=rwx,o=rx
mrg@rhino: <b>umask 0022</b>
mrg@rhino: <b>umask -S</b>
u=rwx,g=rx,o=rx
</code></pre>

---

# Common Unix Commands

Unix has developed a wide array of tools for working with text files.  These commands are made even more useful by their ability to be chained together using pipes and redirection (which we will discuss shortly).

**wc**

word count- counts lines, words, and characters in a file

**sum**

**md5sum**

performs a checksum on the file.  Time consuming for larger files. `md5sum` is the more thorough version.

**grep**

finds a pattern in a file

**awk**

super-powerful pattern scanning and text processing tool

---

# Common Unix Commands ...

**sed**

stream editor for filtering and transforming text

**less/more**

paginators for displaying files.  `less` has more features than `more`.

**head/tail**

displays the first/last few lines of a file

**file**

shows a file's type- useful for determining if a file is binary or text.

---

# Common Unix Commands ...

**man**

**info**

Contains documentation on the command.  Try `man` first- `info` has never really taken on, but some commands have the full documentation in that form.

The command is run as `man <command>`, with the command's manual page displayed in the terminal.

---

# Input and Output

**STDIN**: short for "standard input"- where a process takes input from

**STDOUT**: sort for "standard output"- where output will be displayed.

**STDERR**: where (well-behaved) programs display diagnostic, error, or warning messages.

By default, these sources and sinks all use the terminal you're running the command in.  STDIN is from your keyboard, STDOUT and STDERR to the terminal.

---

# Input and Output ...

## Redirection

Most commonly we end up redirecting output.  The greater-than symbol "`>`" redirects standard output to the target "pointed at" by that symbol:

    cmd --argument > afile.txt

Redirects the output of `cmd` to `afile.txt` in the current directory.  Error messages will still be displayed.

The next most common use of redirection is to chain commands together using pipes:

    cmd --arguments | grep 'pattern'

In this case, the STDOUT from `cmd` is piped into grep, which searches for `pattern` and displays the lines found on the terminal we're running in.

Another common use is to throw away error messages or capture them into a file:

    cmd --arguments 2>/dev/null
    cmd --arguments 2>error.file

---

# Input and Output ...

## Redirection ...

<pre><code>
mrg@rhino$ <b>pwd</b>
/home/mrg/unix-101
mrg@rhino$ <b>ls</b>
bin  data  README
mrg@rhino$ <b>cat README > foo</b>
mrg@rhino$ <b>sum README foo</b>
52074     1 README
52074     1 foo
mrg@rhino$ <b>cat README | grep et</b>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar blandit
tristique. Morbi euismod arcu et pulvinar accumsan. Nulla volutpat placerat
risus, nec vulputate purus commodo vel. Nam consectetur leo vitae viverra
lobortis lectus interdum in. Nullam leo sapien, aliquet id magna at, porta
</code></pre>

---

# Input and Output ...

## Redirection ...

<pre><code>
mrg@rhino$ <b>ls bin/env.{sh,env,dne}</b>
ls: cannot access bin/env.dne: No such file or directory
bin/env.env  bin/env.sh
mrg@rhino$ <b>ls bin/env.{sh,env,dne} 2>errors.txt</b>
bin/env.env  bin/env.sh
mrg@rhino$ <b>cat errors.txt </b>
ls: cannot access bin/env.dne: No such file or directory
mrg@rhino$ <b>ls bin/env.{sh,env,dne} > output.txt 2>errors.txt</b>
mrg@rhino$ <b>cat output.txt </b>
bin/env.env
bin/env.sh
mrg@rhino$ <b>cat errors.txt </b>
ls: cannot access bin/env.dne: No such file or directory

</code></pre>

---

# Input and Output ...

## Redirection ...

Redirecting using the `>` symbol will overwrite any existing file (basically, erase and write anew).  To append, use the double-arrow operator `>>`:

<code><pre>
mrg@rhino$ <b>head -2 README > foo</b>
mrg@rhino$ <b>cat foo</b>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar blandit
tristique. Morbi euismod arcu et pulvinar accumsan. Nulla volutpat placerat
mrg@rhino$ <b>head -1 README > foo</b>
mrg@rhino$ <b>cat foo</b>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar blandit
mrg@rhino$ <b>head -1 README >> foo</b>
mrg@rhino$ <b>cat foo</b>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar blandit
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar blandit
mrg@rhino$ 
</pre></code>


---

# The Shell Environment

Earlier we mentioned that an _environment variable_ called `$PATH` controls where the shell looks for executables.  `PATH` is one of many variables that control the behavior of the shell.  The command `env` will show you variables and their values:

<pre><code>
mrg@rhino: <b>env |head -10</b>
MODULE_VERSION_STACK=3.2.10
MANPATH=/usr/local/man:/home/mrg/apps/x86_64-ubuntu-linux-12.04/share/man:/usr/share/man
PROFILED=yes
TIME=real: %e\nuser: %U\nsys: %S
PVM_RSH=/usr/bin/ssh
TERM=xterm-256color
SHELL=/bin/bash
BASHRC=yes
XDG_SESSION_COOKIE=ccd8020e9410b9e30b36ef9100000113-1397757867.245232-469362302
mrg@rhino: 
</code></pre>

---

# The Shell Environment ...

The value of an environment variable can be viewed by prepending a dollar-sign to the variable name:

<pre><code>
mrg@rhino$ <b>echo $PWD</b>
/home/mrg/unix-101
mrg@rhino$ <b>echo $HOME</b>
/home/mrg
</code></pre>

Variables are application specific- used by commands to alter their behavior.  The shell has a built-in command called `time` that is used to measure how long a command runs.

<pre><code>
mrg@rhino$ <b>time pwd</b>
/home/mrg

real    0m0.000s
user    0m0.000s
sys 0m0.000s
</code></pre>

---

# The Shell Environment ...

We can change how `time` prints by setting the environment variable called `TIMEFORMAT` (check the manpage for `bash`):

<pre><code>
mrg@rhino$ <b>echo $TIMEFORMAT</b>

mrg@rhino$ <b>time pwd</b>
/home/mrg

real    0m0.000s
user    0m0.000s
sys 0m0.000s
mrg@rhino$ <b>TIMEFORMAT='real: %R user: %U sys: %S'</b>
mrg@rhino$ <b>time pwd</b>
/home/mrg
real: 0.000 user: 0.000 sys: 0.000

</code></pre>

---

# The Shell Environment ...

Variables exist only for the current shell:

<pre><code>
mrg@rhino$ <b>TIMEFORMAT='real: %R user: %U sys: %S'</b>
mrg@rhino$ <b>time sleep 1</b>
real: 1.014 user: 0.000 sys: 0.000
mrg@rhino$ <b>echo $TIMEFORMAT</b>
real: %R user: %U sys: %S
mrg@rhino$ <b>bash</b>
mrg@rhino$ <b>echo $TIMEFORMAT</b>

mrg@rhino$ <b>time sleep 1</b>

real    0m1.001s
user    0m0.000s
sys 0m0.000s
mrg@rhino$ <b>exit</b>
mrg@rhino$ <b>echo $TIMEFORMAT</b>
real: %R user: %U sys: %S

</code></pre>

---

# The Shell Environment ...

Exporting a variable makes it available to programs that are started within the shell:

<pre><code>
mrg@rhino$ <b>echo $TIMEFORMAT</b>
real: %R user: %U sys: %S
mrg@rhino$ <b>export TIMEFORMAT</b>
mrg@rhino$ <b>bash</b>
mrg@rhino$ <b>echo $TIMEFORMAT</b>
real: %R user: %U sys: %S
mrg@rhino$ <b>time sleep 1</b>
real: 1.001 user: 0.000 sys: 0.000

</code></pre>

These variables only exist in the current shell and any shells started by this shell- if you want these to persist we need to add those to the shell start up routine.

---

# The Shell Environment ...

## Shell Startup

All shells have a certain startup procedure- typically involving reading so-called *dot-files* in your home directory.  Here you can:

  - set a persistent `umask`
  - set environment variables
  - set terminal and shell behavior (via `set`)



---

# Environment Modules

The `modules` package is a toolset that enables easy management of important environment variables when you need to use certain research software tools maintained by SciComp.


<pre><code>
mrg@rhino$ <b>ls -l /app |head -14</b>
total 312
drwxr-xr-x  3 root root  4096 Sep  9 09:23 anaconda
drwxr-xr-x  2 root root  4096 Nov 21 13:18 arb
drwxr-xr-x  3 root root  4096 Sep  3 13:48 aspera-connect
drwxr-xr-x  3 root root  4096 Oct 28 13:33 bcl2fastq
drwxr-xr-x  3 root root  4096 Sep  3 13:49 bedops
drwxr-xr-x  3 root root  4096 Oct  8 14:51 bedtools
drwxr-xr-x  5 root root  4096 May 17  2013 benchmark
</code></pre>

For example, if you want to use `bcl2fastq`, run the command:

<pre><code>
mrg@rhino$ module load bcl2fastq
</code></pre>

---

# Environment Modules ...

The `modules` command updates your environment with needed environment variables to use the default version of bcl2fastq:

<pre><code>
mrg@rhino$ <b>module show bcl2fastq</b>
-------------------------------------------------------------------
/app/Modules/modulefiles/bcl2fastq/1.8.4:

module-whatis    Illumina bcl2fastq 1.8.4 
prepend-path     PATH /app/perl/5.14.4/bin 
prepend-path     PATH /app/bcl2fastq/1.8.4/bin 
-------------------------------------------------------------------
mrg@rhino$ echo $PATH
<u>/app/bcl2fastq/1.8.4/bin:/app/perl/5.14.4/bin</u>:/app/Modu ....
</code></pre>

---

# Environment Modules ...

<pre><code>
mrg@rhino$ <b>module avail | head -10</b>

---------------------------- /app/Modules/versions -----------------------------
3.2.10

----------------------- /app/Modules/3.2.10/modulefiles ------------------------
dot         module-git  module-info modules     use.own

--------------------------- /app/Modules/modulefiles ---------------------------
anaconda/2.0.1         java/jdk1.7.0_25       python2/2.7.8
aspera-connect/3.1.1   java/jdk1.7.0_51       python3/3.4.1
bcl2fastq/1.8.4        julia/0.3.0            R/3.0.1
bedops/2.4.2           king/1.4               R/3.1.1
bedtools/2.21.0        lastz/1.02.00          R/3.1.2(default)
blasr/2014.09.30       littler/0.1.5          R/devel
blast/2.2.26           locuszoom/1.1          rclone/1.05
blat/34                matlab/R2013b          rstudio/0.97.173
boost/1.44.0           matlab/R2014a(default) rstudio/0.97.551
boost/1.55.0           matlab/R2014b          rstudio/0.98.1060
boost/1.56.0           MEME/4.10.0            rsync/3.1.1
bowtie/1.1.0           Modules/3.2.10         rtf2latex/2.2.2
bowtie2/2.2.3          NAMD/2.9               sage/6.3
bwa/0.7.10             ncbi-blast/2.2.28+     samtools/1.0
casava/1.8.2           nco/4.4.5              scadmin/1.0.0
clustalw/2.1           netcdf/4.3.2           seqtools/4.29
cuda/cuda-5.0          new_fugue/2010-06-02   sratoolkit/2.3.4-2
cuda/cuda-5.5          novocraft/3.02.02      sratoolkit/2.4.2
cufflinks/2.2.1        null                   stata/13
cutadapt/1.1           olb/1.9.4              tophat/2.0.12
FastQC/0.11.2          perl/5.14.4            tpp/4.7.1
genetorrent/3.8.6      phase/2.1.1            ucsc/2014_01_21
gmap-gsnap/2014-08-20  plink/1.90             vmd/1.9.1
igv/2.3.26             plinkseq/0.08          zlib/1.2.8
IGVTools/2.3.26        psipred/3.4
java/jdk1.6.0_45       python/default
</code></pre>

<pre><code>
</code></pre>

---

# Text Editors

There are a number of text editors available- the biggest are `nano`, `vi`, and `emacs`.

* `nano` is a very easy editor to use, very similar to Windows text editors, but note that the mouse isn't used
* `emacs` is very popular with developers.  Super rich feature set, lots of capabilities for integrating with devtools
* `vi` is the go-to for sysadmins.  Relatively lightweight, typically built in to all distributions, but certainly not intuitive.  Takes some practice.

If you have a graphical environment, Ubuntu provides a text editor called `gedit`.

---

# Shell Scripts

Any command can be put into a text file and run later:

<pre><code>
mrg@rhino$ <b>cat bin/env.sh</b>
#!/usr/bin/env bash
#SBATCH -J imajob
echo TMPDIR $TMPDIR
echo SCRATCH_LOCAL $SCRATCH_LOCAL
echo SCRATCH $SCRATCH
echo DELETE30 $DELETE30
</code></pre>

> Anything after the "#" (hash or sharp) is ignored by the shell... *except*
> for the first line, which tells the shell which interpreter to use.  The
> `/usr/bin/env bash` is good practice- while bash is typically in the same
> place all the time, using this format is more portable, especially as you
> start to write scripts in other languages (e.g. python, R, or perl)

---

# The Secure Shell

There are a variety of Unix-based computing hosts around the Hutch.  `ssh` is the most common method for accessing shells on these hosts. 

`PuTTY` is a Windows SSH client that provides a terminal.  On OSX and Linux systems, ssh is available from the command line.

<pre><code>
mrg@ahost$ <b>ssh rhino.fhcrc.org</b>
Welcome to Ubuntu 12.04.3 LTS (GNU/Linux 3.5.0-43-generic x86_64)
############################################################
                    IMPORTANT NOTES:
         (************ UPDATED ************)
* This computer is intended for interactive scientific computing, 
  development and for compute jobs that require > 20GB memory.

* If you are running a long & CPU intensive job (> 1h) and your
  job requires less than 20GB of memory you can start an inter-
  active session to Gizmo by using the 'grab*' commands to get 
  immediate access to a compute server with 1-12 cores.
Last login: Thu Apr 17 13:41:25 2014 from dhcp180157.fhcrc.org
mrg@rhino04[~]: 

</code></pre>

`ssh` has a companion- `scp` which allows you to copy files using the same secure session

---

# Available Resources

## NX Servers

The hosts `lynx` and `manx` run the NX server software suite.  These systems provide login services and can be used for general computing tasks such as email, basic file management, access to other resources (e.g. rhino and gizmo) document editing, etc.

_Please do not_ use this system for compute-intensive tasks such as compliling large software packages or bioinfomatic tasks.  These are comparitively small hosts that will not handle that sort of load.

---

# Available Resources ...

## `rhino` Compute Nodes

The `rhino` compute nodes are large memory, shared systems.  These are systems intended for:

* interactive work
* prototyping and development
* compiling software
* jobs requiring more than 48GB RAM

Access to these systems is via secure shell (`ssh`).  There are four rhino nodes- when you use the alias `rhino`, a round-robin system distributes your session to any one of the four nodes.

### Use

These systems should not be used for intensive computational tasks unless the task _requires_ significant memory (i.e. greater than 48GB).  Other tasks should be limited in quantity and in run-time.  Do not run multiple jobs (more than 10) or run them for a significant amount of time (1,000 CPU-seconds).


---

# Available Resources ...

## `gizmo`

`gizmo` is the compute cluster that you should use for most "production" computational tasks.  Use of the cluster is a topic for another course, but you can try it by using one of the `grabnode` commands on either the NX servers or one of the rhino nodes.

Access to `gizmo` nodes requires the use of Slurm commands- Slurm is the software suite that queues and dispatches jobs to the cluster.

---

# Available Resources ...

## Storage

Storage comes in three basic flavors:

 * fast
 * economy
 * secure

Each of those "flavors" is mounted on all of the gizmo compute systems- compute nodes and rhino nodes alike.

<pre><code>
mrg@rhino$ <b>ls -l /fh</b>
total 4
drwxr-xr-x 270 root root    0 Nov  4 21:11 economy
drwxr-xr-x 271 root root    0 Nov 19 11:39 fast
dr-xr-xr-x   3 root root 4096 Nov  2 13:09 secure
mrg@rhino$ <b>ls -l /fh/fast/corey_l/scicomp</b>
total 13
drwxrwxr-x 2 mrg g_mrg    52 Oct 29 10:42 bcl2fastq
drwxrwsr-x 4 mrg SciComp 123 Nov 17 14:16 build
drwxrwsr-x 4 mrg SciComp  53 Aug 25 09:14 econofile
drwxrwsr-x 3 mrg SciComp 144 Oct 16 08:40 filenames
drwxrwsr-x 3 mrg SciComp  21 Nov 17 14:50 work
</code></pre>

---

# Available Resources ...

## Storage ...

* use "fast" for data you're actively working on.
* use "secure" for data that requires a higher level of security
* use "economy" for data that's no longer required for daily activity

[CIT Description of storage systems ]( https://teams.fhcrc.org/sites/centerit/pages/service-details.aspx?service=27 )




---

# Available Resources ...

* email: scicomp@fhcrc.org
* [These Slides: https://teams.fhcrc.org/sites/citwiki/SciComp/Training%20Materials/Unix%20101/unix-101.html](https://teams.fhcrc.org/sites/citwiki/SciComp/Training%20Materials/Unix%20101/unix-101.html)
* [SciComp Docs: http://scicomp.fhcrc.org](http://scicomp.fhcrc.org)
* [Unix Tutorial: http://www.ee.surrey.ac.uk/Teaching/Unix/ ](http://www.ee.surrey.ac.uk/Teaching/Unix/)
* [Software Carpentry: http://software-carpentry.org/](http://software-carpentry.org/)
* [Bash Guide for Beginners: http://www.tldp.org/LDP/Bash-Beginners-Guide/html/](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/)

---

# Sample Data Sources

**Short fastq SRA data sample**

> http://brianknaus.com/software/srtoolbox/s_4_1_sequence80.txt

**PDB Sample**

> Structure-guided design, synthesis and biological evaluation of novel DNA ligase inhibitors with in vitro and in vivo anti-staphylococcal activity. 
(2012) Bioorg.Med.Chem.Lett. 22: 6705-6711

> http://www.rcsb.org/pdb/explore/explore.do?structureId=4GLX

# Credits

These slides were developed using Markdown, Vim, and Landslide


