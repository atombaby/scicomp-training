#!/bin/bash

echo "Starting job at $(date)"

if [ "${SLEEP_SECONDS}" == "" ]
then
    sleep 60
else
    sleep "${SLEEP_SECONDS}"
fi

echo "Complete"
