#!/usr/bin/env bash

R_HOME=/app/R/2.15.2/lib64/R
R_PROFILE=${R_HOME}/library/Rmpi/Rprofile; export R_PROFILE

R CMD BATCH ${SLURM_SUBMIT_DIR}/rmpi.hello.R
X=$?

echo $0 "exited with return code" $X
exit $X

