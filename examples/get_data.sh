#!/bin/bash
mkdir -p unix-101/data/fastq
mkdir -p unix-101/data/pdb

wget http://brianknaus.com/software/srtoolbox/s_4_1_sequence80.txt \
    -O unix-101/data/fastq/s_4_1_sequence80.txt

wget http://www.rcsb.org/pdb/explore/explore.do?structureId=4GLX \
    -O unix-101/data/pdb/dna-ligase.pdb

cd unix-101/data/pdb
wget -o heptane.pdb http://xray.bmc.uu.se/hicup/HP6/hp6_clean.pdb
wget -o decane.pdb http://xray.bmc.uu.se/hicup/D10/d10_clean.pdb
wget -o methane.pdb http://xray.bmc.uu.se/hicup/MSM/msm_clean.pdb

echo "Done"
